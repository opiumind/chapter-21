package problem5;

import java.io.*;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

/**
 * (Syntax highlighting) Write a program that converts a Java file into an HTML
 file. In the HTML file, the keywords, comments, and literals are displayed in
 bold navy, green, and blue, respectively. Use the command line to pass a Java file
 and an HTML file. For example, the following command
 java Exercise21_05 Welcome.java Welcome.html
 converts Welcome.java into Welcome.html. Figure 21.8a shows a Java file. The
 corresponding HTML file is shown in Figure 21.8b.

 !Consider that there is no regular expressions in the source file.
 *
 */
public class App 
{
    public static void main( String[] args ) {
        Scanner input = new Scanner(System.in);
        File source, result;
        String readFile;
        String resultText;
        BufferedWriter bw = null;
        FileWriter fw = null;

        System.out.println( "Please enter the path of the java file:" );
        source = new File(input.nextLine());
        try (Scanner fileText = new Scanner(source)) {
            readFile = fileText.useDelimiter("\\Z").next();
            resultText = highlightKeywordsAndComments(readFile);
            resultText = highlightLiterals(resultText);


            System.out.println( "Please enter the path of the html file:" );
            result = new File(input.nextLine());
            try {
                fw = new FileWriter(result.getAbsoluteFile());
                bw = new BufferedWriter(fw);

                bw.write(resultText);

                System.out.println("Html file is ready!");
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (bw != null)
                        bw.close();

                    if (fw != null)
                        fw.close();

                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        } catch (FileNotFoundException e) {
            System.out.println("This java file was not found");
        }
    }

    public static String highlightKeywordsAndComments(String sourceText) {
        String[] keywordString = {"abstract", "assert", "boolean",
                "break", "byte", "case", "catch", "char", "class", "const",
                "continue", "default", "do", "double", "else", "enum",
                "extends", "for", "final", "finally", "float", "goto",
                "if", "implements", "import", "instanceof", "int",
                "interface", "long", "native", "new", "package", "private",
                "protected", "public", "return", "short", "static",
                "strictfp", "super", "switch", "synchronized", "this",
                "throw", "throws", "transient", "try", "void", "volatile",
                "while", "true", "false", "null"};
        Set<String> keywordSet = new HashSet<>(Arrays.asList(keywordString));
        StringBuilder result = new StringBuilder();
        result.append("<pre>");
        StringBuilder word = new StringBuilder();
        boolean isOneLineComment = false;
        boolean isMultilineComment = false;

        for (int i = 0; i < sourceText.length(); i++) {
            if (sourceText.charAt(i) == '/') {
                if (sourceText.charAt(i+1) == '/') {
                    result.append("<span style='color: blue'>");
                    isOneLineComment = true;
                } else if (sourceText.charAt(i+1) == '*') {
                    result.append("<span style='color: blue'>");
                    isMultilineComment = true;
                }
            } else if (sourceText.charAt(i) == '\n' && isOneLineComment) {
                result.append("</span>");
                isOneLineComment = false;
            } else if (i > 1 && sourceText.charAt(i-2) == '*' && sourceText.charAt(i-1) == '/' && isMultilineComment) {
                result.append("</span>");
                isMultilineComment = false;
            }

            if (!isOneLineComment && !isMultilineComment && (sourceText.charAt(i) == ' ' || sourceText.charAt(i) == '\n')) {
                if (keywordSet.contains(word.toString())) {
                    result.insert(result.length() - word.length(),
                            "<span style='font-weight:bold; color: navy'>");
                    result.append("</span>");
                }
                word = new StringBuilder();
            } else {
                word.append(sourceText.charAt(i));
            }

            result.append(sourceText.charAt(i));

        }
        result.append("</pre>");
        return result.toString();
    }

    public static String highlightLiterals(String sourceText) {
        StringBuilder result = new StringBuilder();
        boolean isLiteral = false;
        boolean isDigitLiteral = false;
        boolean needToAppend = true;
        boolean isOneLineComment = false;
        boolean isMultilineComment = false;

        for (int i = 0; i < sourceText.length(); i++) {
            if (sourceText.charAt(i) == '/') {
                if (sourceText.charAt(i+1) == '/') {
                    isOneLineComment = true;
                } else if (sourceText.charAt(i+1) == '*') {
                    isMultilineComment = true;
                }
            } else if (sourceText.charAt(i) == '\n' && isOneLineComment) {
                isOneLineComment = false;
            } else if (i > 1 && sourceText.charAt(i-2) == '*' && sourceText.charAt(i-1) == '/' && isMultilineComment) {
                isMultilineComment = false;
            }
            if (!isOneLineComment && !isMultilineComment) {
                if (sourceText.charAt(i) == '"') {
                    if (isLiteral && sourceText.charAt(i - 1) != '\\') {
                        result.append(sourceText.charAt(i));
                        needToAppend = false;
                        result.append("</span>");
                        isLiteral = false;
                    } else if (!isLiteral) {
                        isLiteral = true;
                        result.append("<span style='color: green'>");
                    }
                }
                if (String.valueOf(sourceText.charAt(i)).matches("\\d") &&
                        String.valueOf(sourceText.charAt(i - 1)).matches("[^a-zA-Z0-9]") &&
                        !isDigitLiteral && !isLiteral) {
                    result.append("<span style='color: green'>");
                    isDigitLiteral = true;
                }

                if (isDigitLiteral && String.valueOf(sourceText.charAt(i)).matches("[^a-zA-Z0-9]")) {
                    result.append("</span>");
                    isDigitLiteral = false;
                }

                if (needToAppend) {
                    result.append(sourceText.charAt(i));
                }
                needToAppend = true;
            } else {
                result.append(sourceText.charAt(i));
            }

        }

        return result.toString();
    }
}
