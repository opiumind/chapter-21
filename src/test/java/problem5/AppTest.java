package problem5;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class AppTest 
    extends TestCase
{
    public void testHighlightKeywordsAndComments() throws Exception {
        assertEquals("Wrong keywords highlighting.", "<pre>@Autowired\n" +
                "    <span style='font-weight:bold; color: navy'>private</span> UserValidator userValidator;\n" +
                "\n" +
                "    <span style='color: blue'>// inject via application.properties</span>\n" +
                "    @Value(\"${main.message:test}\")\n" +
                "    <span style='font-weight:bold; color: navy'>private</span> String message = \"Hello World\";</pre>", App.highlightKeywordsAndComments("@Autowired\n" +
                "    private UserValidator userValidator;\n" +
                "\n" +
                "    // inject via application.properties\n" +
                "    @Value(\"${main.message:test}\")\n" +
                "    private String message = \"Hello World\";"));
    }

    public void testHighlightLiterals() throws Exception {
        assertEquals("Wrong literals highlighting.", "@RequestMapping(value = {<span style='color: green'>\"/welcome\"</span>}, method = RequestMethod.GET)\n" +
                "    public String welcome(Map<String, Object> model) {\n" +
                "        model.put(<span style='color: green'>\"message\"</span>, this.message);\n" +
                "        return <span style='color: green'>\"welcome\"</span>;\n" +
                "    }", App.highlightLiterals("@RequestMapping(value = {\"/welcome\"}, method = RequestMethod.GET)\n" +
                "    public String welcome(Map<String, Object> model) {\n" +
                "        model.put(\"message\", this.message);\n" +
                "        return \"welcome\";\n" +
                "    }"));
    }


    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp()
    {
        assertTrue( true );
    }
}
